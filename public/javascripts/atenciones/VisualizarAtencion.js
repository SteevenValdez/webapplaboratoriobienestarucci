﻿const urlHttp = '/api/atenciones';
const urlHttpPacientes = '/api/pacientes';

let urlActual = window.location.href;
if (urlActual.slice(-1) == "/") {
    urlActual = urlActual.substring(0, urlActual.length - 1);
}
const idAtencion = urlActual.substr(urlActual.lastIndexOf('/') + 1);

let newMixin = {
    created: function () {
        this.getAtencionesWithId();
    },
    data: {
        fechaatencion: '',
        tipoatencion: '',
        medico: '',
        diagnostico: '',
        observacion: '',        
        tipoconsulta: '',
        cantidad: '',
        atenciones: [],
        errorM: [],
        mensaje: '',
        mostrarBotonModificar: '',
        rules: {
            required: (value) => !!value || 'Requerido.',
            paciente: (value) => {
                if (value == '' || !value) return 'Este campo es obligatorio';
                return true;
            },
            fechaatencion: (value) => {
                if (!value) return 'Este campo es obligatorio';
                return true;
            },
            tipoatencion: (value) => {
                if (!value) return 'Este campo es obligatorio';
                return true;
            },
            medico: (value) => {
                if (!value) return 'Este campo es obligatorio';
                if (value && value.length > 100) return 'Este campo no deben ser mayor de 100 caracteres';
                return true;
            },
            diagnostico: (value) => {
                if (!value) return 'Este campo es obligatorio';
                if (value && value.length > 254) return 'Este campo no deben ser mayor de 254 caracteres';
                return true;
            },            
            observacion: (value) => {
                if (!value) return 'Este campo es obligatorio';
                if (value && value.length > 254) return 'Este campo no deben ser mayor de 254 caracteres';
                return true;
            },
            tipoconsulta: (value) => {
                if (!value) return 'Este campo es obligatorio';
                return true;
            },
            cantidad: (value) => {
                if (!value) return 'Este campo es obligatorio';
                if (value && value.length > 10) return 'Este campo no deben ser mayor de 10 caracteres';
                return true;
            }
        },
        tipoAtencionItems: [
            { text: 'Normal' },
            { text: 'Personalizada' }
        ],
        tipoConsultaItems: [
            { text: 'Interna' },
            { text: 'Externa' }
        ],        
        modalfechaatencion: false,
        snackbar: false,
        context: '',
        mode: 'vertical',
        timeout: 7000,
        cargando: false,
        itemspaciente: [],
        buscarpaciente: null,
        paciente: []
    },
    watch: {
        buscarpaciente(val) {

            let queryParams = {
                params: {
                    q: val,
                    pagina: 1,
                    nresultados: 20
                }
            };

            val && this.getPacientesWithParams(queryParams)
        }
    },
    methods: {
        getAtencionesWithId: function () {
            this.cargando = true;
            this.$http.get(urlHttp + '/' + idAtencion).then(
                response => {
                    //Esto ocurre cuando la peticion ha sido realizada exitosamente.
                    const atencion = response.body;

                    const inicialFechaAtencion = atencion.FechaAtencion.split("/");
                    const fechaAtencion = inicialFechaAtencion[2] + '-' + inicialFechaAtencion[1] + '-' + inicialFechaAtencion[0];

                    this.fechaatencion = fechaAtencion;
                    this.tipoatencion = atencion.TipoAtencion;
                    this.medico = atencion.Medico;
                    this.diagnostico = atencion.Diagnostico;
                    this.observacion = atencion.Observacion;
                    this.tipoconsulta = atencion.TipoConsulta;
                    this.cantidad = atencion.Cantidad;
                    this.getPacientesPorID(atencion.Paciente);
                    
                    this.mostrarBotonModificar = true;
                    this.cargando = false;

                }, response => {
                    //Esto ocurre cuando ha existido un error al realizar la petición.
                    this.context = 'error';
                    this.mensaje = response.body.Message;
                    this.snackbar = true;
                    this.mostrarBotonModificar = false;
                    this.cargando = false;
                }
            );
        },
        getPacientesWithParams: function (queryParams) {
            this.cargando = true;
            this.$http.get(urlHttpPacientes, queryParams).then(
                response => {
                    //Esto ocurre cuando la peticion ha sido realizada exitosamente.
                    let pacientesNoLocal = response.body.rows;
                    let pacientesLocal = [];
                    for (var i = 0; i < pacientesNoLocal.length; i++) {
                        let paciente = {
                            id: pacientesNoLocal[i].id,
                            IdentificacionYNombreCompleto: pacientesNoLocal[i].Identificacion + ' - ' + pacientesNoLocal[i].NombreCompleto
                        }
                        pacientesLocal.push(paciente);
                    }
                    this.itemspaciente = pacientesLocal;
                    this.cargando = false;
                }, response => {
                    //Esto ocurre cuando ha existido un error al realizar la petición.
                    this.itemspaciente = [];
                    this.cargando = true;
                }
            );
        },
        getPacientesPorID: function (idPaciente) {
            this.$http.get(urlHttpPacientes + '/' + idPaciente).then(
                response => {
                    //Esto ocurre cuando la peticion ha sido realizada exitosamente.
                    let pacientesNoLocal = response.body;
                    let pacientesLocal = [];
                    let paciente = {
                        id: pacientesNoLocal.id,
                        IdentificacionYNombreCompleto: pacientesNoLocal.Identificacion + ' - ' + pacientesNoLocal.NombreCompleto
                    }
                    pacientesLocal.push(paciente);
                    this.itemspaciente = pacientesLocal;
                    this.paciente = idPaciente;
                }, response => {
                    //Esto ocurre cuando ha existido un error al realizar la petición.
                    this.context = 'error';
                    this.mensaje = response.body.Message;
                    this.snackbar = true;
                    this.mostrarBotonModificar = false;
                }
            );
        }
    }
};