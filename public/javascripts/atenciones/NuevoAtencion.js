﻿const urlHttp = '/api/atenciones';
const urlHttpPacientes = '/api/pacientes';

let newMixin = {
    data: {
        fechaatencion: '',
        tipoatencion: '',
        medico: '',
        diagnostico: '',
        observacion: '',        
        tipoconsulta: '',
        cantidad: '',
        atenciones: [],
        errorM: [],
        mensaje: '',
        rules: {
            required: (value) => !!value || 'Requerido.',
            paciente: (value) => {
                if (value == '' || !value) return 'Este campo es obligatorio';
                return true;
            },
            fechaatencion: (value) => {
                if (!value) return 'Este campo es obligatorio';
                return true;
            },
            tipoatencion: (value) => {
                if (!value) return 'Este campo es obligatorio';
                return true;
            },
            medico: (value) => {
                if (!value) return 'Este campo es obligatorio';
                if (value && value.length > 100) return 'Este campo no deben ser mayor de 100 caracteres';
                return true;
            },
            diagnostico: (value) => {
                if (!value) return 'Este campo es obligatorio';
                if (value && value.length > 254) return 'Este campo no deben ser mayor de 254 caracteres';
                return true;
            },            
            observacion: (value) => {
                if (!value) return 'Este campo es obligatorio';
                if (value && value.length > 254) return 'Este campo no deben ser mayor de 254 caracteres';
                return true;
            },
            tipoconsulta: (value) => {
                if (!value) return 'Este campo es obligatorio';
                return true;
            },
            cantidad: (value) => {
                if (!value) return 'Este campo es obligatorio';
                if (value && value.length > 10) return 'Este campo no deben ser mayor de 10 caracteres';
                return true;
            }
        },
        tipoAtencionItems: [
            { text: 'Normal' },
            { text: 'Personalizada' }
        ],
        tipoConsultaItems: [
            { text: 'Interna' },
            { text: 'Externa' }
        ],        
        modalfechaatencion: false,
        snackbar: false,
        context: '',
        mode: 'vertical',
        timeout: 7000,
        cargando: false,
        itemspaciente: [],
        buscarpaciente: null,
        paciente: []
    },
    watch: {
        buscarpaciente(val) {

            let queryParams = {
                params: {
                    q: val,
                    pagina: 1,
                    nresultados: 20
                }
            };

            val && this.getAtencionesWithParams(queryParams)
        }
    },
    methods: {
        getAtencionesWithParams: function (queryParams) {
            this.cargando = true;
            this.$http.get(urlHttpPacientes, queryParams).then(
                response => {
                    //Esto ocurre cuando la peticion ha sido realizada exitosamente.
                    let pacientesNoLocal = response.body.rows;
                    let pacientesLocal = [];
                    for (var i = 0; i < pacientesNoLocal.length; i++) {
                        let paciente = {
                            id: pacientesNoLocal[i].id,
                            IdentificacionYNombreCompleto: pacientesNoLocal[i].Identificacion + ' - ' + pacientesNoLocal[i].NombreCompleto
                        }
                        pacientesLocal.push(paciente);
                    }
                    this.itemspaciente = pacientesLocal;
                    this.cargando = false;
                }, response => {
                    //Esto ocurre cuando ha existido un error al realizar la petición.
                    this.itemspaciente = [];
                    this.cargando = true;
                }
            );
        },
        postAtenciones: function () {
            this.$http.post(
                urlHttp,
                {
                    FechaAtencion: this.fechaatencion,
                    TipoAtencion: this.tipoatencion,
                    Medico: this.medico,
                    Diagnostico: this.diagnostico,
                    Observacion: this.observacion,
                    TipoConsulta:this.tipoconsulta,
                    Cantidad: this.cantidad,
                    Paciente: this.paciente
                }
            ).then(
                response => {
                    //Esto ocurre cuando la peticion ha sido realizada exitosamente.
                    this.context = 'success';
                    this.errorM = response;
                    this.mensaje = response.body.Message;
                    this.snackbar = true;
                },
                response => {
                    //Esto ocurre cuando ha existido un error al realizar la petición.
                    this.context = 'error';
                    this.mensaje = response.body.Message;
                    this.snackbar = true;
                });
        },
        clearFields: function () {
            this.fechaatencion = '';
            this.tipoatencion = '';
            this.medico = '';
            this.diagnostico = '';
            this.observacion = '';
            this.tipoconsulta = '';
            this.cantidad = '';
            this.paciente = '';
        },
    }
};