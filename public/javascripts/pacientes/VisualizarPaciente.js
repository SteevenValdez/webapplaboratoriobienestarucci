﻿const urlHttp = '/api/pacientes';
let urlActual = window.location.href;
if (urlActual.slice(-1) == "/") {
    urlActual = urlActual.substring(0, urlActual.length - 1);
}
const idPaciente = urlActual.substr(urlActual.lastIndexOf('/') + 1);

let newMixin = {
    created: function () {
        this.getPacientesWithId();
    },
    data: {
        identificacion: '',
        nombres: '',
        apellidos: '',
        genero: '',
        fechanacimiento: '',
        email: '',
        genero: '',
        telefono: '',
        gruposanguineo: '',
        pacientes: [],
        errorM: [],        
        generoItems: [
            { text: 'Masculino' },
            { text: 'Femenino' }
        ],
        grupoSanguineoItems: [
            { text: 'O-' },
            { text: 'O+' },
            { text: 'A-' },
            { text: 'A+' },
            { text: 'B-' },
            { text: 'B+' },
            { text: 'AB-' },
            { text: 'AB+' },

        ],
        snackbar: false,
        context: '',
        mensaje: '',
        mode: 'vertical',
        timeout: 7000
    },
    methods: {
        getPacientesWithId: function () {            
            this.$http.get(urlHttp + '/' + idPaciente).then(
                response => {
                    //Esto ocurre cuando la peticion ha sido realizada exitosamente.
                    const paciente = response.body;

                    const inicialFechaNacimiento = paciente.FechaNacimiento.split("/");
                    const fechaNacimiento = inicialFechaNacimiento[2] + '-' + inicialFechaNacimiento[1] + '-' + inicialFechaNacimiento[0];                    

                    this.identificacion = paciente.Identificacion;
                    this.nombres = paciente.Nombres;
                    this.apellidos = paciente.Apellidos;
                    this.genero = paciente.Genero;                    
                    this.fechanacimiento = fechaNacimiento;
                    this.gruposanguineo = paciente.GrupoSanguineo;
                    this.email = paciente.Email;
                    this.telefono = paciente.Telefono;
                }, response => {
                    //Esto ocurre cuando ha existido un error al realizar la petición.
                    this.context = 'error';
                    this.mensaje = response.body.Message;
                    this.snackbar = true;
                }
            );
        }
    }
};