﻿const urlHttp = '/api/pacientes';
let urlActual = window.location.href;
if (urlActual.slice(-1) == "/") {
    urlActual = urlActual.substring(0, urlActual.length - 1);
}

let newMixin = {
    created: function () {
        this.getPacientesWithOutParams();
    },
    data: {
        mostrarIconoCrear: true,
        mostrarIconoDetalle: false,
        mostrarIconoEditar: false,
        mostrarIconoEliminar: false,
        mostrarIconoActualizar: true,
        mostrarIconoBuscar: true,
        mostrarTextoBuscar: false,
        mostrarIconoHacerBuscar: false,
        mostrarIconoCerrarBuscar: false,
        permitirCambios: true,
        valorBuscar: '',
        search: '',
        numberOfRows: 5,
        selectNumberOfRows: [
            { text: 5 },
            { text: 10 },
            { text: 20 },
            { text: 50 }
        ],
        actualPage: 1,
        pagination: {
            sortBy: 'Identificacion',
            page: 1,
            rowsPerPage: 5
        },
        selected: [],
        headers: [
            {
                text: 'Identificación',
                value: 'Identificacion',
            },
            {
                text: 'Nombres',
                value: 'Nombres',
            },
            {
                text: 'Apellidos',
                value: 'Apellidos',
            }
            //{
            //    text: 'Nombre Completo',
            //    value: 'nombrecompleto'
            //},
            //{
            //    text: 'Género',
            //    value: 'genero'
            //},
            //{
            //    text: 'Fecha de Nacimiento',
            //    value: 'fechanacimiento'
            //},
            //{
            //    text: 'Edad',
            //    value: 'edad'
            //},
            //{
            //    text: 'Grupo Sanguineo',
            //    value: 'gruposanguineo'
            //},
            //{
            //    text: 'Email',
            //    value: 'email'
            //},
            //{
            //    text: 'Teléfono',
            //    value: 'telefono'
            //}
        ],
        items: [],
        countItems: 0,
        dialogoEliminar: false,
        snackbar: false,
        context: '',
        mensaje: '',
        mode: 'vertical',
        timeout: 7000
    },
    watch: {
        actualPage: function (val, oldVal) {
            if (this.permitirCambios) {
                this.changeActualPage(val);
            }
        },
        numberOfRows: function (val, oldVal) {
            if (this.permitirCambios) {
                this.changeRowCount(val);
            }
        }
    },
    computed: {
        pages() {
            return this.numberOfRows ? Math.ceil(this.countItems / this.numberOfRows) : 0
        }
    },
    methods: {
        getPacientesWithOutParams: function () {
            this.$http.get(urlHttp).then(
                response => {
                    //Esto ocurre cuando la peticion ha sido realizada exitosamente.
                    this.items = response.body.rows;
                    this.countItems = response.body.count;
                    this.pagination.page = 1;
                }, response => {
                    //Esto ocurre cuando ha existido un error al realizar la petición.
                    this.context = 'error';
                    this.mensaje = response.body.Message;
                    this.snackbar = true;
                }
            );
        },
        getPacientesWithParams: function (queryParams) {
            this.$http.get(urlHttp, queryParams).then(
                response => {
                    //Esto ocurre cuando la peticion ha sido realizada exitosamente.
                    //alert(response.body.rows);
                    this.items = response.body.rows;
                    this.countItems = response.body.count;
                    this.pagination.page = 1;
                    if (this.items.length == 0 && this.actualPage > 1) {
                        this.actualPage = this.actualPage - 1;
                    }
                }, response => {
                    //Esto ocurre cuando ha existido un error al realizar la petición.
                    this.context = 'primary';
                    this.mensaje = response.body.Message;
                    this.snackbar = true;
                }
            );
        },
        resetPacientes: function () {
            this.permitirCambios = false;

            let promesa = new Promise((resolve, reject) => {
                try {
                    this.valorBuscar = '';
                    this.actualPage = 1;
                    this.numberOfRows = 5;
                    resolve();
                } catch (e) {
                    reject(e);
                }
            });

            promesa.then(() => {
                this.permitirCambios = true;
                this.pagination.rowsPerPage = this.numberOfRows;
                this.getPacientesWithOutParams();

            }).catch((response) => {
                this.permitirCambios = true;
                // Catch de la ejecución de la promesa.
            });
        },
        detallePaciente: function () {
            window.location.href = urlActual + '/ver/' + this.selected[0].id;
        },
        modificarPaciente: function () {
            window.location.href = urlActual + '/modificar/' + this.selected[0].id;
        },
        eliminarPaciente: function () {
            let idPacientes = [];
            for (let i = 0; i < this.selected.length; i++) {
                idPacientes.push(this.selected[i].id);
            }

            this.dialogoEliminar = false;

            this.$http.delete(urlHttp, {
                body: {
                    id: idPacientes
                }
            }).then(
                response => {
                    //Esto ocurre cuando la peticion ha sido realizada exitosamente.
                    this.selected = [];
                    this.getPacientesWithOutParams();
                    this.context = 'success';
                    this.mensaje = response.body.Message;
                    this.snackbar = true;
                    this.toggle();
                    
                }, response => {
                    //Esto ocurre cuando ha existido un error al realizar la petición.
                    this.context = 'error';
                    this.mensaje = response.body.Message;
                    this.snackbar = true;
                }
            );
        },
        makeSearch: function () {
            if (this.valorBuscar != '') {

                this.permitirCambios = false;

                let promesa = new Promise((resolve, reject) => {
                    try {
                        this.actualPage = 1;
                        this.numberOfRows = 5;

                        resolve();
                    } catch (e) {
                        reject(e);
                    }
                });

                promesa.then(() => {
                    this.permitirCambios = true;

                    let queryParams = {
                        params: {
                            q: this.valorBuscar,
                            pagina: this.actualPage,
                            nresultados: this.numberOfRows
                        }
                    }

                    this.pagination.rowsPerPage = this.numberOfRows;

                    this.getPacientesWithParams(queryParams);

                }).catch((response) => {
                    this.permitirCambios = true;
                    // Catch de la ejecución de la promesa.
                });
            }
            else {
                alert('Ingrese un valor antes de hacer la búsqueda');
            }
        },
        changeRowCount: function (numberOfRows) {

            let promise = new Promise((resolve, reject) => {
                try {
                    let queryParams = {
                        params: {
                            pagina: this.actualPage,
                            nresultados: numberOfRows
                        }
                    }

                    if (this.valorBuscar != '') {
                        queryParams.params.q = this.valorBuscar;
                    }

                    this.pagination.page = this.actualPage;
                    this.pagination.rowsPerPage = this.numberOfRows;

                    this.getPacientesWithParams(queryParams);
                    resolve();
                } catch (e) {
                    reject(e);
                }
            });

            promise.then(() => {
                // Asegurar la ejecución de la promesa.
            }).catch(() => {
                // Catch de la ejecución de la promesa.
            });
        },
        changeActualPage: function (actualPage) {
            let queryParams = {
                params: {
                    pagina: actualPage,
                    nresultados: this.numberOfRows
                }
            }

            if (this.valorBuscar != '') {
                queryParams.params.q = this.valorBuscar;
            }

            this.getPacientesWithParams(queryParams);
        },
        showSearchInput: function () {
            this.mostrarTextoBuscar = true;
            this.mostrarIconoBuscar = false;
            this.mostrarIconoHacerBuscar = true;
            this.mostrarIconoCerrarBuscar = true;
            this.mostrarIconoCrear = false;
            this.mostrarIconoActualizar = false;
        },
        hideSearchInput: function () {
            this.mostrarTextoBuscar = false;
            this.mostrarIconoBuscar = true;
            this.mostrarIconoHacerBuscar = false;
            this.mostrarIconoCerrarBuscar = false;
            this.mostrarIconoCrear = true;
            this.mostrarIconoActualizar = true;
        },
        toggleAll: function () {
            if (this.selected.length) this.selected = []
            else this.selected = this.items.slice()

            this.toggle();
        },
        toggle: function () {
            if (this.selected.length) {
                this.mostrarIconoCrear = false;
                this.mostrarIconoActualizar = false;
                this.mostrarIconoBuscar = false;
                this.mostrarIconoEliminar = true;
                this.mostrarIconoBuscar = false;
                this.mostrarTextoBuscar = false;
                this.mostrarIconoHacerBuscar = false;
                this.mostrarIconoCerrarBuscar = false;
                if (this.selected.length == 1) {
                    this.mostrarIconoEditar = true;
                    this.mostrarIconoDetalle = true;
                }
                else {
                    this.mostrarIconoEditar = false;
                    this.mostrarIconoDetalle = false;
                }
            }
            else {
                this.mostrarIconoCrear = true;
                this.mostrarIconoDetalle = false;
                this.mostrarIconoEliminar = false;
                this.mostrarIconoEditar = false;
                this.mostrarIconoActualizar = true;
                this.mostrarIconoBuscar = true;
                this.mostrarTextoBuscar = false;
                this.mostrarIconoHacerBuscar = false;
                this.mostrarIconoCerrarBuscar = false;
            }
        },
        changeSort: function (column) {
            if (this.pagination.sortBy === column) {
                this.pagination.descending = !this.pagination.descending
            } else {
                this.pagination.sortBy = column
                this.pagination.descending = false
            }
        }
    }
};