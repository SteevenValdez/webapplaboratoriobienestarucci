﻿const urlHttp = '/api/pacientes';
let urlActual = window.location.href;
if (urlActual.slice(-1) == "/") {
    urlActual = urlActual.substring(0, urlActual.length - 1);
}
const idPaciente = urlActual.substr(urlActual.lastIndexOf('/') + 1);

let newMixin = {
    created: function () {
        this.getPacientesWithId();
    },
    data: {
        identificacion: '',
        nombres: '',
        apellidos: '',
        genero: '',
        fechanacimiento: '',
        email: '',
        genero: '',
        telefono: '',
        gruposanguineo: '',
        pacientes: [],
        errorM: [],
        mostrarBotonModificar: '',
        rules: {
            required: (value) => !!value || 'Requerido.',
            identificacion: (value) => {
                if (!value) return 'Este campo es obligatorio';
                if (value && value.length > 25) return 'Este campo no debe ser mayor de 25 caracteres';
                return true;
            },
            nombres: (value) => {
                if (!value) return 'Este campo es obligatorio';
                if (value && value.length > 30) return 'Este campo no debe ser mayor de 30 caracteres';
                return true;
            },
            apellidos: (value) => {
                if (!value) return 'Este campo es obligatorio';
                if (value && value.length > 30) return 'Este campo no deben ser mayor de 30 caracteres';
                return true;
            },
            genero: (value) => {
                if (!value) return 'Este campo es obligatorio';
                return true;
            },
            fechanacimiento: (value) => {
                if (!value) return 'Este campo es obligatorio';
                return true;
            },
            gruposanguineo: (value) => {
                if (!value) return 'Este campo es obligatorio';
                return true;
            },
            email: (value) => {
                if (!value) return 'Este campo es obligatorio';
                if (value && value.length > 100) return 'Este campo no deben ser mayor de 100 caracteres';
                const pattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                return pattern.test(value) || 'E-mail Invalido.';
            },
           telefono: (value) => {
                if (!value) return 'Este campo es obligatorio';
                if (value && value.length > 10) return 'Este campo no deben ser mayor de 10 caracteres';
                return true;
            }
        },
        generoItems: [
            { text: 'Masculino' },
            { text: 'Femenino' }
        ],
        grupoSanguineoItems: [
            { text: 'O-' },
            { text: 'O+' },
            { text: 'A-' },
            { text: 'A+' },
            { text: 'B-' },
            { text: 'B+' },
            { text: 'AB-' },
            { text: 'AB+' },

        ],        
        modalfechanacimiento: false,
        snackbar: false,
        context: '',
        mensaje: '',
        mode: 'vertical',
        timeout: 7000
    },
    methods: {
        getPacientesWithId: function () {            
            this.$http.get(urlHttp + '/' + idPaciente).then(
                response => {
                    //Esto ocurre cuando la peticion ha sido realizada exitosamente.
                    const paciente = response.body;

                    const inicialFechaNacimiento = paciente.FechaNacimiento.split("/");
                    const fechaNacimiento = inicialFechaNacimiento[2] + '-' + inicialFechaNacimiento[1] + '-' + inicialFechaNacimiento[0];                    

                    this.identificacion = paciente.Identificacion;
                    this.nombres = paciente.Nombres;
                    this.apellidos = paciente.Apellidos;
                    this.genero = paciente.Genero;                    
                    this.fechanacimiento = fechaNacimiento;
                    this.gruposanguineo = paciente.GrupoSanguineo;
                    this.email = paciente.Email;
                    this.telefono = paciente.Telefono;
                    this.mostrarBotonModificar = true;

                }, response => {
                    //Esto ocurre cuando ha existido un error al realizar la petición.
                    this.context = 'error';
                    this.mensaje = response.body.Message;
                    this.snackbar = true;
                    this.mostrarBotonModificar = false;
                }
            );
        },
        putPaciente: function () {
            this.$http.put(
                urlHttp + '/' + idPaciente,
                {
                    Identificacion: this.identificacion,
                    Nombres: this.nombres,
                    Apellidos: this.apellidos,
                    Genero: this.genero,
                    FechaNacimiento: this.fechanacimiento,
                    GrupoSanguineo: this.gruposanguineo,
                    Email: this.email,
                    Telefono: this.telefono
                }
            ).then(
                response => {
                    //Esto ocurre cuando la peticion ha sido realizada exitosamente.
                    this.context = 'success';
                    this.errorM = response;
                    this.mensaje = response.body.Message;
                    this.snackbar = true;
                },
                response => {
                    //Esto ocurre cuando ha existido un error al realizar la petición.
                    this.context = 'error';
                    this.mensaje = response.body.Message;
                    this.snackbar = true;
                });
        },
        clearFields: function () {
            this.identificacion = '';
            this.nombres = '';
            this.apellidos = '';
            this.genero = '';
            this.fechanacimiento = '';
            this.gruposanguineo = '';
            this.email = '';
            this.telefono = '';
        }
    }
};