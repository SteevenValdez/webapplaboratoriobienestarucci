﻿const urlHttp = '/api/pacientes';

let newMixin = {
    data: {
        identificacion: '',
        nombres: '',
        apellidos: '',
        genero: '',
        fechanacimiento: '',
        email: '',
        genero: '',
        telefono: '',
        gruposanguineo: '',
        pacientes: [],
        errorM: [],
        mensaje: '',
        rules: {
            required: (value) => !!value || 'Requerido.',
            identificacion: (value) => {
                if (!value) return 'Este campo es obligatorio';
                if (value && value.length > 25) return 'Este campo no debe ser mayor de 25 caracteres';
                return true;
            },
            nombres: (value) => {
                if (!value) return 'Este campo es obligatorio';
                if (value && value.length > 30) return 'Este campo no debe ser mayor de 30 caracteres';
                return true;
            },
            apellidos: (value) => {
                if (!value) return 'Este campo es obligatorio';
                if (value && value.length > 30) return 'Este campo no deben ser mayor de 30 caracteres';
                return true;
            },
            genero: (value) => {
                if (!value) return 'Este campo es obligatorio';
                return true;
            },
            fechanacimiento: (value) => {
                if (!value) return 'Este campo es obligatorio';
                return true;
            },
            gruposanguineo: (value) => {
                if (!value) return 'Este campo es obligatorio';
                return true;
            },
            email: (value) => {
                if (!value) return 'Este campo es obligatorio';
                if (value && value.length > 100) return 'Este campo no deben ser mayor de 100 caracteres';
                const pattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                return pattern.test(value) || 'E-mail Invalido.';
            },
           telefono: (value) => {
                if (!value) return 'Este campo es obligatorio';
                if (value && value.length > 10) return 'Este campo no deben ser mayor de 10 caracteres';
                return true;
            }
        },
        generoItems: [
            { text: 'Masculino' },
            { text: 'Femenino' }
        ],
        grupoSanguineoItems: [
            { text: 'O-' },
            { text: 'O+' },
            { text: 'A-' },
            { text: 'A+' },
            { text: 'B-' },
            { text: 'B+' },
            { text: 'AB-' },
            { text: 'AB+' },

        ],        
        modalfechanacimiento: false,
        snackbar: false,
        context: '',
        mode: 'vertical',
        timeout: 7000
    },
    methods: {
        postPacientes: function () {
            this.$http.post(
                urlHttp,
                {
                    Identificacion: this.identificacion,
                    Nombres: this.nombres,
                    Apellidos: this.apellidos,
                    Genero: this.genero,
                    FechaNacimiento: this.fechanacimiento,
                    GrupoSanguineo:this.gruposanguineo,
                    Email: this.email,
                    Telefono: this.telefono
                }
            ).then(
                response => {
                    //Esto ocurre cuando la peticion ha sido realizada exitosamente.
                    this.context = 'success';
                    this.errorM = response;
                    this.mensaje = response.body.Message;
                    this.snackbar = true;
                },
                response => {
                    //Esto ocurre cuando ha existido un error al realizar la petición.
                    this.context = 'error';
                    this.mensaje = response.body.Message;
                    //this.$refs.snackbar.open();
                    this.snackbar = true;
                });
        },
        clearFields: function () {
            this.identificacion = '';
            this.nombres = '';
            this.apellidos = '';
            this.genero = '';
            this.fechanacimiento = '';
            this.gruposanguineo = '';
            this.email = '';
            this.telefono = '';
        }
    }
};