﻿$(document).ready(function() {
    $('#tabla-atenciones').DataTable( {
        language: {
            url: 'http://cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json'
        },
        responsive: {
            details: {
                display: $.fn.dataTable.Responsive.display.modal( {
                    header: function ( row ) {
                        var data = row.data();
                        return 'Details for '+data[0]+' '+data[1];
                    }
                } ),
                renderer: $.fn.dataTable.Responsive.renderer.tableAll( {
                    tableClass: 'table'
                } )
            }
        }
    } );
} );