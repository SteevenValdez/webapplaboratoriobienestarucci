﻿Vue.use(Vuetify);

let VueApp = new Vue({
    el: '#vue-app',
    mixins: [newMixin],
    data: {
        drawer: true,
        itemsNavigation: [
            { icon: 'trending_up', text: 'Pacientes', href: '/formularios/Pacientes' },
            { icon: 'subscriptions', text: 'Atenciones', href: '/formularios/Atenciones' }
        ]
    }
});