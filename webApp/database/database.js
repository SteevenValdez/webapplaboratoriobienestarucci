﻿const Sequelize = require('sequelize');

const config = require('../../config/config');
const configBD = require('../../config/database');

let sequelize = new Sequelize(configBD.NombreBaseDeDatos, configBD.UsuarioPostgres, configBD.ContrasenaPostgres, {
    host: configBD.DireccionPostgress,
    port: configBD.PuertoPostgres,
    dialect: 'postgres',

    pool: {
        max: 5,
        min: 0,
        idle: 10000
    }
});

module.exports = sequelize;