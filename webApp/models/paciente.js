﻿"use strict";

const Moment = require('moment');

module.exports = function (sequelize, DataTypes) {
    let Paciente = sequelize.define('Paciente', {
        id: {
            type: DataTypes.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        Identificacion: {
            type: DataTypes.STRING,
            unique: true,
            allowNull: false
        },
        Nombres: {
            type: DataTypes.STRING,
            allowNull: false
        },
        Apellidos: {
            type: DataTypes.STRING,
            allowNull: false
        },
        NombreCompleto: {
            type: DataTypes.VIRTUAL,
            get: function () {
                var nombre = this.getDataValue('Nombres');
                var apellido = this.getDataValue('Apellidos');
                return [nombre, apellido].join(' ');
            }
        },
        Genero: {
            type: DataTypes.STRING,
            allowNull: false
        },
        FechaNacimiento: {
            type: DataTypes.DATEONLY,
            allowNull: false,
            get: function () {
                return Moment(this.getDataValue('FechaNacimiento')).format('DD/MM/YYYY');
            }
        },
        //Atenciones: {
        //    type: DataTypes.INTEGER,
        //    references: {
        //        model: "Atencions",
        //        key: "id"
        //    }
        //},
        Edad: {
            type: DataTypes.VIRTUAL,
            get: function () {
                var fechaActual = Moment(Moment().format('YYYY-MM-DD'));
                var fechaNacimiento = Moment(this.getDataValue('FechaNacimiento'));
                return fechaActual.diff(fechaNacimiento, 'years');
            }
        },
        GrupoSanguineo: {
            type: DataTypes.STRING,
            allowNull: false
        },
        Email: {
            type: DataTypes.STRING,
            allowNull: false
        },
        Telefono: {
            type: DataTypes.STRING
        }
    },
        { underscored: true }
    );
    
    return Paciente;
};