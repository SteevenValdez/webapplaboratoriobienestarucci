﻿const Sequelize = require('sequelize');

let fs = require("fs");
let path = require("path");

const config = require('../../config/config');
const configBD = require('../../config/database');

let sequelize = new Sequelize(configBD.NombreBaseDeDatos, configBD.UsuarioPostgres, configBD.ContrasenaPostgres, {
    host: configBD.DireccionPostgress,
    port: configBD.PuertoPostgres,
    dialect: 'postgres',

    pool: {
        max: 5,
        min: 0,
        idle: 10000
    }
});

let db = {};

fs
    .readdirSync(__dirname)
    .filter(function (file) {
        return (file.indexOf(".") !== 0) && (file !== "index.js");
    })
    .forEach(function (file) {
        var model = sequelize.import(path.join(__dirname, file));
        db[model.name] = model;
    });

Object.keys(db).forEach(function (modelName) {
    if ("associate" in db[modelName]) {
        db[modelName].associate(db);
    }
});

db.sequelize = sequelize;
db.Sequelize = Sequelize;

//Relations

db.Paciente.hasMany(db.Atencion, {
    foreignKey: {
        name: 'Paciente',
        allowNull: false
    }});
//db.Paciente.belongsTo(db.Atencion);
//db.Atencion.belongsTo(db.Paciente);

module.exports = db;