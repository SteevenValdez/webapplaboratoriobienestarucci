﻿'use strict';

const Moment = require('moment');

module.exports = function (sequelize, DataTypes) {
    let Atencion = sequelize.define('Atencion', {
        id: {
            type: DataTypes.INTEGER,
            autoIncrement: true,
            primaryKey: true,
        },
        FechaAtencion: {
            type: DataTypes.DATEONLY,
            allowNull: false,
            get: function () {
                return Moment(this.getDataValue('FechaAtencion')).format('DD/MM/YYYY');
            }
        },
        TipoAtencion: {
            type: DataTypes.STRING,
            allowNull: false
        },
        Medico: {
            type: DataTypes.STRING,
            allowNull: false
        },
        Diagnostico: {
            type: DataTypes.STRING,
            allowNull: false
        },
        Observacion: {
            type: DataTypes.STRING,
            allowNull: false
        },
        TipoConsulta: {
            type: DataTypes.STRING,
            allowNull: false
        },
        Cantidad: {
            type: DataTypes.INTEGER,
            allowNull: false
        }
    });

    return Atencion;
};



