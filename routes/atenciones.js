﻿'use strict';
var express = require('express');
var router = express.Router();

const models = require('../webapp/models');

router.get('/nuevo', function (req, res) {
    res.render('NuevoAtencion', { title: 'Crear Nueva Atención' });
});

router.get('/modificar/:id', (req, res) => {
    res.render('ModificarAtencion', { title: 'Modificar Atención' });
});

router.get('/ver/:id', (req, res) => {
    res.render('VisualizarAtencion', { title: 'Ver detalles de la Atención' });
});

module.exports = router;