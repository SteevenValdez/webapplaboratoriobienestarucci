﻿'use strict';
const express = require('express');
const router = express.Router();

const models = require('../webapp/models');

//const Moment = require('moment');

router.get('/pacientes/:id', (req, res) => {
    const idPaciente = req.params.id;
    models.Paciente.findOne({ where: { id: idPaciente } }).then(paciente => {
        if (paciente && paciente != null) {
            res.status(200).json(paciente);
        }
        else {
            res.status(403).json({ Message: 'No se ha encontrado el paciente.' })
        }
    });
});

router.get('/pacientes', (req, res) => {
    const textoABuscar = req.query.q;
    let paginaABuscar = req.query.pagina;
    let numeroDeResultados = req.query.nresultados;
    let numeroDeResultadosOmitidos = null;    
    let queryParams = {};

    if (textoABuscar && paginaABuscar && numeroDeResultados) {        
        numeroDeResultadosOmitidos = (paginaABuscar * numeroDeResultados) - numeroDeResultados;

        // El operador $iLike solo funciona con Postgres
        const whereQuery = {
            $or: [
                {
                    Identificacion: {
                        $iLike: '%' + textoABuscar + '%'
                    }
                },
                {
                    Nombres: {
                        $iLike: '%' + textoABuscar + '%'
                    }
                },
                {
                    Apellidos: {
                        $iLike: '%' + textoABuscar + '%'
                    }
                }
            ]
        };

        queryParams = {
            offset: numeroDeResultadosOmitidos,
            limit: numeroDeResultados,
            where: whereQuery,
            //include: models.Atencion
        };
        consultarPacientes(queryParams);
    }
    else {
        if (!paginaABuscar && !numeroDeResultados) {
            paginaABuscar = 1;
            numeroDeResultados = 5;
        }

        numeroDeResultadosOmitidos = (paginaABuscar * numeroDeResultados) - numeroDeResultados;

        queryParams = {
            offset: numeroDeResultadosOmitidos,
            limit: numeroDeResultados,
            //include: models.Atencion
        };     
        consultarPacientes(queryParams);
    }

    function consultarPacientes(queryParams) {
        models.Paciente.findAndCountAll(queryParams).then(pacientes => {
            if (pacientes && pacientes.count > 0) {
                res.status(200).json(pacientes);
            }
            else {
                res.status(403).json({ Message: 'No se han encontrado pacientes.' })
            }
        });
    }
});

router.post('/pacientes', (req, res) => {
    if (req.body == null || req.body.Identificacion == null || req.body.Nombres == null || req.body.Apellidos == null || req.body.Genero == null || req.body.FechaNacimiento == null || req.body.GrupoSanguineo == null || req.body.Email == null || req.body.Telefono == null) {
        res.status(403).json({ Message: 'No nos estan llegando los datos, por favor verifique los datos antes de enviarlos.'});
    }
    else {
        //var inicialFechaNacimiento = req.body.FechaNacimiento.split("/");
        //var fechaNacimiento = inicialFechaNacimiento[2] + '/' + inicialFechaNacimiento[1] + '/' + inicialFechaNacimiento[0];
        let newPaciente = {
            Identificacion: req.body.Identificacion,
            Nombres: req.body.Nombres,
            Apellidos: req.body.Apellidos,
            Genero: req.body.Genero,
            FechaNacimiento: req.body.FechaNacimiento,
            GrupoSanguineo: req.body.GrupoSanguineo,
            Email: req.body.Email,
            Telefono: req.body.Telefono
        };
        models.Paciente.findOrCreate({ where: { Identificacion: newPaciente.Identificacion }, defaults: newPaciente })
            .spread((paciente, created) => {
                if (paciente == null && created == null) {
                    res.status(500).json({ Message: 'Ha ocurrido un error a la hora de procesar la solicitud' });
                }
                else {
                    let message = null;
                    if (created) {
                        message = 'El paciente se ha creado correctamente.';

                    }
                    else {
                        message = 'El paciente que intenta crear ya existe.';
                    }
                    console.log(message);
                    res.status(200).json({ Message: message, Paciente: paciente });
                }
            });
    }
});

router.put('/pacientes/:id', (req, res) => {
    if (req.body == null) {
        res.status(403).json({ Message: 'No nos estan llegando los datos, por favor verifique los datos antes de enviarlos.' });
    }
    else {
        models.Paciente.findOne({ where: { id: req.params.id } }).then(paciente => {
            if (paciente) {
                paciente.Identificacion = req.body.Identificacion;
                paciente.Nombres = req.body.Nombres;
                paciente.Apellidos = req.body.Apellidos;
                paciente.Genero = req.body.Genero;
                paciente.FechaNacimiento = req.body.FechaNacimiento;
                paciente.GrupoSanguineo = req.body.GrupoSanguineo;
                paciente.Email = req.body.Email;
                paciente.Telefono = req.body.Telefono;

                paciente.save().then(paciente => {
                    if (paciente) {
                        res.status(200).json({ Message: 'El paciente ha sido modificado exitosamente', Paciente: paciente });
                    }
                    else {
                        res.status(500).json({ Message: 'Ha ocurrido un error al tratar de guardar las modificaciones del paciente' });
                    }
                }).catch(err => {
                    res.status(500).json({ Message: 'Ha ocurrido un error al tratar de guardar las modificaciones del paciente'});
                });
            }
            else {
                res.status(404).json({ Message: 'No se ha encontrado el paciente que quiere modificar' });
            }
        });
    }
});

router.delete('/pacientes', (req, res) => {
    let idPacientes = req.body.id;

    //El operador $any solo funciona en Postgres
    if (idPacientes.length > 0 || idPacientes != null) {
        models.Paciente.destroy({
            where: {
                id: {
                    $any: idPacientes
                }
            }
        }).then(() => {
            res.status(200).json({ Message: 'El paciente se ha eliminado correctamente' });
        })
            .catch(() => {
                res.status(500).json({ Message: 'Ha ocurrido un error al tratar de eliminar el paciente' });
            });
    }
    else {
        res.status(403).json({ Message: 'Ha ocurrido un error al llegarnos los datos. Por favor intente de nuevo.' });
    }        
});

router.get('/atenciones/:id', (req, res) => {
    const idAtencion = req.params.id;
    models.Atencion.findOne({ where: { id: idAtencion } }).then(atencion => {
        if (atencion && atencion != null) {
            res.status(200).json(atencion);
        }
        else {
            res.status(403).json({ Message: 'No se ha encontrado el registro de atención.' })
        }
    });
});

router.get('/atenciones', (req, res) => {
    const textoABuscar = req.query.q;
    let paginaABuscar = req.query.pagina;
    let numeroDeResultados = req.query.nresultados;
    let numeroDeResultadosOmitidos = null;
    let queryParams = {};

    if (textoABuscar && paginaABuscar && numeroDeResultados) {
        numeroDeResultadosOmitidos = (paginaABuscar * numeroDeResultados) - numeroDeResultados;

        // El operador $iLike solo funciona con Postgres
        const whereQuery = {
            $or: [
                {
                    TipoAtencion: {
                        $iLike: '%' + textoABuscar + '%'
                    }
                },
                {
                    Medico: {
                        $iLike: '%' + textoABuscar + '%'
                    }
                }
            ]
        };

        queryParams = {
            offset: numeroDeResultadosOmitidos,
            limit: numeroDeResultados,
            where: whereQuery
        };
        consultarPacientes(queryParams);
    }
    else {
        if (!paginaABuscar && !numeroDeResultados) {
            paginaABuscar = 1;
            numeroDeResultados = 5;
        }

        numeroDeResultadosOmitidos = (paginaABuscar * numeroDeResultados) - numeroDeResultados;

        queryParams = {
            offset: numeroDeResultadosOmitidos,
            limit: numeroDeResultados
        };
        consultarPacientes(queryParams);
    }

    function consultarPacientes(queryParams) {
        models.Atencion.findAndCountAll(queryParams).then(atenciones => {
            if (atenciones && atenciones.count > 0) {
                res.status(200).json(atenciones);
            }
            else {
                res.status(403).json({ Message: 'No se han encontrado registros de atenciones.' })
            }
        });
    }
});

router.post('/atenciones', (req, res) => {
    if (req.body == null) {
        res.status(403).json({ Message: 'No nos estan llegando los datos, por favor verifique los datos antes de enviarlos.' });
    }
    else {
        console.log(req.body);
        //var inicialFechaAtencion = req.body.FechaAtencion.split("/");
        //var fechaAtencion = inicialFechaAtencion[2] + '/' + inicialFechaAtencion[1] + '/' + inicialFechaAtencion[0];
        let newAtencion = {
            FechaAtencion: req.body.FechaAtencion,
            TipoAtencion: req.body.TipoAtencion,
            Medico: req.body.Medico,
            Diagnostico: req.body.Diagnostico,
            Observacion: req.body.Observacion,
            TipoConsulta: req.body.TipoConsulta,
            Cantidad: req.body.Cantidad,
            //paciente_id: req.body.paciente_id
            Paciente: req.body.Paciente
        };
        models.Atencion.create(newAtencion).then(() => {
            res.status(200).json({ Message: 'La atención se ha creado correctamente', Atencion: newAtencion });
        }).catch(() => {
            res.status(500).json({ Message: 'Ha ocurrido un error al grabar la atención' });
        });
    }
});

router.put('/atenciones/:id', (req, res) => {
    if (req.body == null) {
        res.status(403).json({ Message: 'No nos estan llegando los datos, por favor verifique los datos antes de enviarlos.' });
    }
    else {
        models.Atencion.findOne({ where: { id: req.params.id } }).then(atencion => {
            if (atencion) {
                atencion.FechaAtencion = req.body.FechaAtencion;
                atencion.TipoAtencion = req.body.TipoAtencion;
                atencion.Medico = req.body.Medico;
                atencion.Diagnostico = req.body.Diagnostico;
                atencion.Observacion = req.body.Observacion;
                atencion.TipoConsulta = req.body.TipoConsulta;
                atencion.Cantidad = req.body.Cantidad;
                atencion.Paciente = req.body.Paciente;

                atencion.save().then(atencion => {
                    if (atencion) {
                        res.status(200).json({ Message: 'La atención ha sido modificado exitosamente', Atencion: atencion });
                    }
                    else {
                        res.status(500).json({ Message: 'Ha ocurrido un error al tratar de guardar las modificaciones de la atención' });
                    }
                }).catch(err => {
                    res.status(500).json({ Message: 'Ha ocurrido un error al tratar de guardar las modificaciones de la atención' });
                });
            }
            else {
                res.status(404).json({ Message: 'No se ha encontrado la atención que quiere modificar' });
            }
        });
    }
});

router.delete('/atenciones', (req, res) => {
    let idAtenciones = req.body.id;

    //El operador $any solo funciona en Postgres
    if (idAtenciones.length > 0 || idAtenciones != null) {
        models.Atencion.destroy({
            where: {
                id: {
                    $any: idAtenciones
                }
            }
        }).then(() => {
            res.status(200).json({ Message: 'La atención se ha eliminado correctamente' });
        })
            .catch(() => {
                res.status(500).json({ Message: 'Ha ocurrido un error al tratar de eliminar la atención' });
            });
    }
    else {
        res.status(403).json({ Message: 'Ha ocurrido un error al llegarnos los datos. Por favor intente de nuevo.' });
    }
});

module.exports = router;