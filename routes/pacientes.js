﻿'use strict';
var express = require('express');
var router = express.Router();

const models = require('../webapp/models');

router.get('/nuevo', function (req, res) {
    res.render('NuevoPaciente', { title: 'Crear Nuevo Paciente' });
});

router.get('/modificar/:id', (req, res) => {
    res.render('ModificarPaciente', { title: 'Modificar Paciente' });
});

router.get('/ver/:id', (req, res) => {
    res.render('VisualizarPaciente', {title: 'Ver detalles del Paciente'});
});

module.exports = router;
