﻿'use strict';
var express = require('express');
var router = express.Router();


const models = require('../webapp/models');

//router.get('/', function (req, res) {
//    res.render('index', { title: 'Sistema UCCI' });
//});

router.get('/pacientes', function (req, res) {
    res.render('TablaPaciente', { title: 'Registro de Pacientes' });
});

router.get('/atenciones', function (req, res) {
    res.render('TablaAtencion', { title: 'Registro de Atenciones' });
});

module.exports = router;
